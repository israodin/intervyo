#!/bin/bash

#set -x
EXPECTED_ARGS=3
E_BADARGS=65
MYSQL=`which mysql`
DBNAME=$1
DBUSER=$2
DBPASS=$3
HOSTNAME=localhost

if [ "$MYSQL" == "" ]
then
        echo "Installing mysql..."
        sudo apt-get install mysql-client
fi

if [ "$4" != "" ]
then
	HOSTNAME=$4
fi

Q1="CREATE DATABASE IF NOT EXISTS $DBNAME;"
Q2="GRANT USAGE ON *.* TO $DBUSER@172.31.1.125 IDENTIFIED BY '$DBPASS';"
Q3="GRANT ALL PRIVILEGES ON $DBNAME.* TO $DBUSER@172.31.1.125;"
Q4="FLUSH PRIVILEGES;"
SQL="${Q1}${Q2}${Q3}${Q4}"

if [ $# -lt $EXPECTED_ARGS ]
then
  echo "Usage: $0 dbname dbuser dbpass [hostname]"
  exit $E_BADARGS
fi