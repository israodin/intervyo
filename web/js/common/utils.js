/**
 * Returns a random integer between min and max
 * Using Math.round() will give you a non-uniform distribution!
 */
function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getCurrentTime(){
    // add a zero in front of numbers<10
    function addTrailingZero(i){
        return i<10 ? "0" + i : i;
    }

    var now = new Date();
    var h=now.getHours();
    var m=now.getMinutes();
    var s=now.getSeconds();
    m = addTrailingZero(m);
    s = addTrailingZero(s);
    return h+":"+m+":"+s;
}

function scrollDown(elt){
    $(elt).animate({
        scrollTop: $(elt).prop("scrollHeight")
    }, 300);
}

/*** EXTENDING NATIVE OBJECTS ****/

/* Native Array */
Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; // for testing purposes
};

Array.prototype.moveToTheEnd = function (index) {
    this.move(index, this.length - 1);
    return this;
};

Array.prototype.isEmpty = function () {
    return this.length < 1;
};

Array.prototype.contains = function (val) {
    for (var i = 0, len = this.length; i < len; i++) {
        if (this[i] == val) return true;
    }
    return false;
};
Array.prototype.haveElementInCommon = function (arr) {
    var res = false;
    this.forEach(function(elt){
        if (arr.contains(elt)){
            res = true;
            return;
        }
    });
    return res;
};
Array.prototype.getRandomElement = function() {
    var index = getRandomInt(0, this.length - 1);
    return this[index];
};

/* Native Strings */
String.prototype.contains = function (s) {
    return this.indexOf(s) > -1;
};

String.prototype.indexOfFirstElementContained = function (stringArr) {
    if (!stringArr || !stringArr.length){
        return -1;
    }
    for (var i=0 ; i<stringArr.length ; i++){
        if (this.indexOf(stringArr[i]) > -1){
            return i;
        }
    }
    return -1;
};

String.prototype.count = function (s) {
    return this.split(s).length - 1;
};

