var HR_FINISHED_TALK_EVT = "HRFinishedToTalk";
var HR_VIDEO_LOADED = "HRVideoLoaded";
var HR_VIDEO_FULLY_LOADED = "HRVideoFullyLoaded";
var HR_VIDEO_PLAY_STARTED = "HRVideoPlayStarted";
var HR_VIDEO_ENDED = "HRVideoEnded";
var LISTENING_STARTED = "ListeningStarted";
var LISTENING_STOPPED = "ListeningStoppped";

/**** EVENT MANAGER for pub/sub ****/
/* Usage:
 // Your code can publish and subscribe to events as:
 EventManager.subscribe("tabClicked", function() {
 // do something
 });

 EventManager.publish("tabClicked");

 EventManager.unsubscribe("tabClicked");
 */
var EventManager = {
    subscribe: function(event, fn) {
        $(this).bind(event, fn);
    },
    unsubscribe: function(event, fn) {
        $(this).unbind(event, fn);
    },
    publish: function(event) {
        $(this).trigger(event);
    }
};
