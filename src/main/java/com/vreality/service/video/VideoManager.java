package com.vreality.service.video;

import com.vreality.model.actors.Video;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by noam - 5/18/14
 */
@Service
public class VideoManager {
    public final static String MP4_FORMAT = "h264";

    private List<Video> videoList = new ArrayList<Video>(){{
        add(new Video("https://intervyo.s3.amazonaws.com/videos/INT-0a.m4v", 19, "", 3800000L, MP4_FORMAT));
    }};

    public List<Video> getVideos(){
        // TODO - make this list of videos coming from DB
        return videoList;
    }

}
