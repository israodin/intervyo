package com.vreality.service.user;

import com.vreality.model.actors.authentication.Account;
import com.vreality.model.interview.Person;

/**
 * Created by noam - 8/17/14
 */
public interface AccountRepository {

    void createAccount(Account account) throws UsernameAlreadyInUseException;

    Account findAccountByUsername(String username);

    Person findPersonByUsername(String username);

}