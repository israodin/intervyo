package com.vreality.service.authentication;

import com.vreality.model.actors.authentication.Account;
import com.vreality.service.user.AccountRepository;
import com.vreality.service.user.UsernameAlreadyInUseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UserProfile;

import java.sql.SQLException;

/**
 * Created by noam - 8/17/14
 */
public class AccountConnectionSignup implements ConnectionSignUp {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private AccountRepository accountRepository;

    public AccountConnectionSignup(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public String execute(Connection<?> connection){
        UserProfile profile = connection.fetchUserProfile();
        Account account = new Account(profile.getEmail(), null, profile.getFirstName(), profile.getLastName());

        try {
            accountRepository.createAccount(account);
        } catch (Exception e) {
            if (e instanceof UsernameAlreadyInUseException) {
                logger.warn("User already registered", e);
            } else if (e instanceof SQLException){
                logger.error("Can't create account for user {}", profile.getName(), e);
            }
            return null;
        }

//        try {
//            personService.savePerson(candidate);
//        } catch (UsernameAlreadyInUseException e) {
//            logger.warn("User already registered", e);
//            return null;
//        } catch (SQLException sqle){
//            logger.error("Can't create account for user {}", profile.getName(), sqle);
//            return null;
//        }
        return account.getUsername();
    }

}