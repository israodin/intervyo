package com.vreality.model.actors.authentication;

/**
 * Created by noam - 8/14/14
 */
public enum SocialProvider {
    facebook,
    linkedin
}