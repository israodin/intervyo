package dao;

import com.vreality.model.interview.Candidate;
import com.vreality.model.interview.Person;
import com.vreality.service.user.PersonService;
import com.vreality.service.user.UsernameAlreadyInUseException;
import org.hibernate.AssertionFailure;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static net.sf.ezmorph.test.ArrayAssertions.assertEquals;

/**
 * Created by noam - 8/13/14
 */
@Test
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/spring-test-config.xml"})
@TransactionConfiguration(defaultRollback = true)
public class CandidatePersistencyTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    PersonService personService;

    @Test
    public void retrieveSavedCandidate() throws UsernameAlreadyInUseException, SQLException{
        String email = "test123@test.com";
        Candidate savedCandidate = new Candidate(email, "z", "Z");
        personService.savePerson(savedCandidate);

        final Person retrievedCandidate = personService.findPersonByUsername(email);

        assertEquals("Retrieved candidate should be the saved one", retrievedCandidate.getEmail(), savedCandidate.getEmail());
    }

    @Test(expectedExceptions = {AssertionFailure.class, UsernameAlreadyInUseException.class})
    public void saveExistingCandidateThrowException() throws UsernameAlreadyInUseException, SQLException{
        String email = "test234@test.com";
        Candidate savedCandidate = new Candidate(email, "a", "A");
        personService.savePerson(savedCandidate);

        Candidate savedCandidateAgain = new Candidate(email, "b", "B");
        personService.savePerson(savedCandidateAgain);
    }

}
